package practice;
import java.util.Scanner;

public class PrimeNos {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num = 0; // Nth number
		int count = 0; // separate counter variable
		
		System.out.print("Number of prime numbers: ");
		num = sc.nextInt();
		
		// array of prime numbers we are going to store later
		// length according to the Nth number
		int[] primes = new int[num];
		
		int test = 0; // to test whether prime or not
		
		// Because the method "checkNumber" returns boolean
		boolean confirm = false; 
		
		while (count < num) {
			
			// checks the "test" variable whether it is prime
			// returns true for prime and false for not prime
			confirm = checkNumber(test);
				
			if (confirm) {
				primes[count] = test; // if "test" is prime, add it to the array of primes
				count += 1; // also update "count" variable to prevent out of bounds error
			}
			
			test += 1; // update "test" variable to test for more numbers
			
			// Only until the array is filled up, then stop looping (see line 22)
		}
		
		// print out all the prime numbers
		for (int j = 0; j < primes.length; j++) {
			System.out.print(primes[j] + ", ");
		}
		
		sc.close();

	}
	
	public static boolean checkNumber(int n) {
		boolean bool = true;
		if (n == 1 || n == 0) {
			bool = false;
		}
		for (int i = n - 1; i > 1; i--) {
			if (n % i == 0) {
				bool = false;
			}
			
		}
		
		return bool;
	}

}
